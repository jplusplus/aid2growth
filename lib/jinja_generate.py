#!/usr/bin/env python3
import jinja2
import markdown
import sys
import os
import csv
import copy
import glob
import bs4

script_path = os.path.dirname(os.path.realpath(__file__))

TEMPLATES_PATH = "templates/"
CONTENT_PATH = "content/sections/"

codes = {}
countrydata = csv.DictReader(
    open(os.path.join(script_path, "../data/country-info.csv"))
)
for row in countrydata:
    codes[row["code3"].lower()] = row["name"]

DFI_EXCLUDES = ("AIIB", "JBIC")

def render_template_into_file(env, templatename, filename, context=None):
    template = env.get_template(templatename)
    if not context:
        context = {}
    html = template.render(**context)
    with open(filename, "wb") as fh:
        fh.write(html.encode("utf-8"))


env = jinja2.Environment(
    loader=jinja2.FileSystemLoader([TEMPLATES_PATH]),
    extensions=["jinja2.ext.with_"],
    trim_blocks=True,
    lstrip_blocks=True,
)


def read_markdown_file(filename):
    print(filename)
    md = markdown.Markdown(extensions=["full_yaml_metadata", "tables"])
    html = md.convert(open(filename, "r").read())
    result = {}
    if md.Meta:
        result.update(md.Meta)
        # result['categories'] = result['taxonomy']['category']
        result["tags"] = result["taxonomy"]["tag"]
        result["original"] = open(filename, "r").read()
        del result["taxonomy"]
    # each meta field value is a list (to preserve linebreaks, see
    # https://python-markdown.github.io/extensions/meta_data/ )
    # we don't want this, so extract the single items
    result["content"] = html
    return result


def mkdir(dirpath):
    # creates a dir if it doesn't exist
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)


countries_reports = {}


def generate_reports():
    mkdir("dist/reports/")
    report_data = csv.DictReader(open("content/reports.csv"))
    articles = []
    for row in report_data:
        article = {
            "title": row["Report title"],
            "authors": row["Author(s)"],
            "institution": row["Institution"],
            "date": row["Date"],
            "description": row["Short description (1 sentence)"],
            "cover_image_url": row["Cover image"],
            "pdf_url": row["Link to PDF"],
            "internal": bool(row["Internal?"]),
            "related_countries": [
                c.strip() for c in row["Related countries"].split(",")
            ],
        }
        articles.append(article)
        for c in article["related_countries"]:
            if not countries_reports.get(c):
                countries_reports[c] = []
            countries_reports[c].append(article)
    pagename = "reports"
    render_template_into_file(
        env,
        "reports.html",
        "dist/reports/index.html",
        {
            "page_name": pagename,
            "page_title": pagename.capitalize(),
            "articles": articles,
        },
    )


def generate_country_pages():
    data = csv.DictReader(open("data/country-info.csv"))
    datadescriptions = csv.DictReader(open("data/country-descriptions.csv"))
    descriptions = {}
    for row in datadescriptions:
        try:
            descriptions[row['country']] = row['description']
        except KeyError:
            print('Error with description for ' + row['country'])
    mkdir("dist/country/")
    # data from world bank with country statistics
    wb_data = csv.DictReader(open("data/worldbank.csv"))
    from pprint import pprint

    cleaned_wb_data = {}
    for row in wb_data:
        country_code = row["Country Code"].lower()
        if country_code not in cleaned_wb_data:
            cleaned_wb_data[country_code] = {}

        series_slugs = {
            "Population, total": "population",
            "Life expectancy at birth, total (years)": "life_expectancy",
            "GDP (current US$)": "gdp",
            "GDP growth (annual %)": "gdp_growth",
            "GDP per capita (current US$)": "gdp_per_capita",
            "Gross capital formation (% of GDP)": "capital_formation",
            "External debt stocks, total (DOD, current US$)": "debt_stocks",
            "Total debt service (% of GNI)": "debt_service",
            "Net migration": "migration",
            "Personal remittances, paid (current US$)": "remittances",
            "Foreign direct investment, net inflows (BoP, current US$)": "investment_inflows",
            "Foreign direct investment, net (BoP, current US$)": "investment",
            "Net ODA received per capita (current US$)": "net_oda",
        }
        key = series_slugs[row["Series Name"]]
        value = row.get("2018 [YR2018]")

        if value == "..":
            value = "No data"
        if "." in value:
            # it's a float
            value = float(value)
            if value < 1000:
                value = "%.2f" % value
            else:
                # large values don't need the decimal part
                value = "{:,d}".format(int(value))
        elif value.isdigit and not value.startswith("No data"):
            # it's an integer, localize it
            value = "{:,d}".format(int(value))
        else:
            # should be a string
            pass

        # Add prefixes or suffixes according to the field
        value = str(value)
        field = row["Series Name"]
        if "No data" not in value:
            if "years" in field:
                value = value + " years"
            elif "%" in field:
                value = value + "%"
            elif "US$" in field:
                value = "$" + value

        # save the value
        cleaned_wb_data[country_code][key] = value

    rows = [row for row in data]
    all_countries = [(row["code3"].lower(), row["name"]) for row in rows]
    for row in rows:
        context = copy.deepcopy(row)
        context["all_countries"] = all_countries
        code = row["code3"].lower()
        if code in ("#N/A", "#n/a"):
            continue
        # add the worldbank data to the context
        try:
            context.update(cleaned_wb_data[code])
        except:
            print()
            print("[!!!] Country not found in World bank database: " + code)
            print()

        photo_file = "country-photos/%s.jpg" % code
        # if os.path.exists(photo_file):
        context["photo"] = photo_file

        context["page_title"] = row["name"]
        context["description"] = descriptions.get(row["name"])
        context["reports"] = countries_reports.get(row["name"])

        mkdir("dist/country/%s/" % code)
        render_template_into_file(
            env, "country.html", "dist/country/%s/index.html" % code, context
        )
    # compare page
    if not os.path.exists("dist/compare/"):
        os.makedirs("dist/compare/")
    render_template_into_file(env, "compare.html", "dist/compare/index.html")


def generate_dfi_pages():
    data = csv.DictReader(open("data/dfi-info.csv"))
    mkdir("dist/dfi/")
    for row in data:
        code = row["slug"]
        if code in DFI_EXCLUDES:
            continue
        context = copy.deepcopy(row)
        photo_file = "img/dfi/%s.jpg" % code
        if os.path.exists(photo_file):
            context["photo"] = photo_file
            print(code)
        context["page_title"] = row["acronym"]

        mkdir("dist/dfi/%s/" % code)
        render_template_into_file(
            env, "dfi.html", "dist/dfi/%s/index.html" % code, context
        )


def generate_embed_pages():
    mkdir("dist/embed/")
    mkdir("dist/embed/country")
    mkdir("dist/embed/compare")
    mkdir("dist/embed/dfi")
    render_template_into_file(env, "embed/embed-main.html", "dist/embed/index.html")
    render_template_into_file(
        env, "embed/embed-country.html", "dist/embed/country/index.html"
    )
    render_template_into_file(
        env, "embed/embed-compare.html", "dist/embed/compare/index.html"
    )
    render_template_into_file(env, "embed/embed-dfi.html", "dist/embed/dfi/index.html")


def generate_static_pages():
    render_template_into_file(env, "index.html", "dist/index.html")
    for pagename in ["about", "methodology", "links"]:
        mkdir("dist/%s/" % pagename)
        context = read_markdown_file("content/%s.md" % pagename)
        if pagename == "links":
            title = "Knowledge Hub"
        else:
            title = pagename.capitalize()
        render_template_into_file(
            env,
            "page.html",
            "dist/%s/index.html" % pagename,
            {
                "page_name": pagename,
                "page_title": title,
                "page_content": context["content"],
            },
        )
    # glossary
    mkdir("dist/glossary/")
    glossary_data = csv.DictReader(open("content/glossary.csv"))
    articles = []
    for row in glossary_data:
        article = {
            "term": row["Term"],
            "definition": row["Definition"],
            # 'source': row['Source'],
        }
        articles.append(article)
    pagename = "glossary"
    render_template_into_file(
        env,
        "glossary.html",
        "dist/glossary/index.html",
        {
            "page_name": pagename,
            "page_title": pagename.capitalize(),
            "articles": articles,
        },
    )
    # team
    mkdir("dist/team/")
    team_data = csv.DictReader(open("content/team.csv"))
    entries = []
    for row in team_data:
        entry = {
            "name": row["Name"],
            "position": row["Position"],
            "photo": row["Photo"],
            "bio": row["Bio (short)"],
            "url": row["Link"],
        }
        entries.append(entry)
    pagename = "team"
    render_template_into_file(
        env,
        "team.html",
        "dist/team/index.html",
        {
            "page_name": pagename,
            "page_title": pagename.capitalize(),
            "entries": entries,
        },
    )


def generate_blog():
    posts = []
    # individual post pages
    for filename in glob.glob("content/posts/**/*.md"):
        context = read_markdown_file(filename)
        slug = context["slug"] = filename.split("/")[-2]
        mkdir("dist/posts/" + slug)
        context["title"] = context["title"].strip("'")
        from dateutil.parser import parse

        context["date"] = parse(context["date"].strip("'"))
        md = markdown.Markdown(extensions=["full_yaml_metadata", "tables"])
        html = md.convert(context["original"])
        soup = bs4.BeautifulSoup(html, "html.parser")
        context["summary"] = " ".join(soup.get_text().split(" ")[:10])
        posts.append(context)
        context["codes"] = codes
        render_template_into_file(
            env, "post.html", "dist/posts/%s/index.html" % context["slug"], context
        )
    from pprint import pprint

    # post listing
    context = {"posts": posts}
    context["codes"] = codes
    render_template_into_file(env, "post-list.html", "dist/posts/index.html", context)


def generate_all():
    generate_reports()
    generate_country_pages()
    generate_dfi_pages()
    generate_embed_pages()
    generate_static_pages()
    generate_blog()


if __name__ == "__main__":
    generate_all()
