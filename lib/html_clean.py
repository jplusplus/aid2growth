#!/usr/bin/env python3
import bs4
import sys
import requests

url = sys.argv[1]
# outfile = sys.argv[2]

html = requests.get(url).content
soup = bs4.BeautifulSoup(html, 'html.parser')

for tag in soup.findAll(True):
    if tag.name in ['style', 'head']:
        tag.extract()
    elif tag.name in ['span']:
        if tag.contents:
            tag.replaceWith(tag.contents[0])
        else:
            tag.extract()
    elif tag.name in ['p'] and not tag.contents:
        tag.extract()
    del tag['class']
    del tag['id']

for tag in soup.findAll(True):
    if tag.name in ['a']:
        tag['href'] = tag['href'].replace('https://www.google.com/url?q=', '').split('&')[0]

# of = open(outfile, 'w')
# of.write(str(soup))
print(soup)

