#!/usr/bin/env python3
import os
import csv
import requests

if not os.path.exists('../img/team/'):
    os.makedirs('../img/team')
if not os.path.exists('../img/reports/'):
    os.makedirs('../img/reports')

data = csv.DictReader(open('../content/team.csv'))
for idx, row in enumerate(data):
    if row.get('Photo'):
        url = row['Photo']
        file_id = row['Photo'].replace('https://drive.google.com/file/d/', '').replace('/view?usp=sharing', '')
        download_url = f"https://drive.google.com/uc?export=download&id={file_id}"
        print(download_url)
        myfile = requests.get(download_url, allow_redirects=True)
        open('../img/team/' + str(idx) + '.jpg', 'wb').write(myfile.content)
data = csv.DictReader(open('../content/reports.csv'))
for idx, row in enumerate(data):
    if row.get('Cover image'):
        url = row['Cover image']
        file_id = row['Cover image'].replace('https://drive.google.com/file/d/', '').replace('/view?usp=sharing', '')
        download_url = f"https://drive.google.com/uc?export=download&id={file_id}"
        print(download_url)
        myfile = requests.get(download_url, allow_redirects=True)
        open('../img/reports/' + str(idx) + '.png', 'wb').write(myfile.content)
