/* Some defaults */
Chart.defaults.global.defaultFontFamily = '"Manrope", Roboto, DejaVu Sans, sans-serif';
Chart.defaults.global.defaultFontStyle = '500';
Chart.defaults.global.defaultFontSize = 12;
Chart.defaults.global.defaultFontColor = '#666';


// Ranking dos países

Papa.parse('/data/aid-per-country.csv', {
  download: true,
  header: true,
  complete: function(res) {

    Papa.parse('/data/country-info.csv', {
      download: true,
      header: true,
      complete: function(results) {
        var countryCodes = {}
        for (let r of results.data) {
          if (r.developing_status == 'Developing') {
            countryCodes[r.name] = r.code3;
          }
        }
        var container = document.getElementById('country-rank');
        for (let row of res.data) {
          if (!row.country) { continue }
          var li = document.createElement('li');
          if (countryCodes[row.country]) {
            li.innerHTML = '<strong><a href="/country/' + countryCodes[row.country].toLowerCase() + '">' + row.country + '</a></strong><span>' + parseFloat(row.total_aid).toFixed(0) + '</span>';
            container.appendChild(li);
          //} else {
            // li.innerHTML = '<strong>' + row.country + '</strong><span>' + parseFloat(row.total_aid).toFixed(0) + '</span>';
          }
        }
      }
    });
  }
});

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
    return color;
}

var ctx = document.getElementById('chart-global');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['2013', '2014', '2015', '2016', '2017'],
    datasets: [{
      label: 'Global investment (yearly)',
      data: [
        30700.206822893,
        36922.5778327114,
        22988.6159308901,
        26486.115122168,
        30140.1038749528,
      ],
      backgroundColor: '#f84919',
      borderColor: '#427aae',
    }],
  },
  options: {
    maintainAspectRatio: false,
    legend: { display: false, },
    scales: {
      xAxes: [{
        gridLines: {
          display: true,
          drawBorder: false,
          color: '#ccc',
          lineWidth: 1,
          zeroLineColor: '#ccc',
          zeroLineWidth: 1,
          borderDash:[3, 5],
         },
        ticks: {
          display: true,
          beginAtZero: true,
          fontStyle: '600',
          fontColor: '#666',
          padding: 10,
        },
      }],
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          beginAtZero: true,
          display: true,
          padding: 5,
        }
      }]
    },
    tooltips: {      
      borderColor: 'transparent',  
      borderWidth: 0,
      cornerRadius: 2,
    }
  }
});

var agencies;
var datasets = [];
Papa.parse('/data/global-investment-agency.csv', {
  download: true,
  header: true,
  complete: function(results) {
    // get agency list from CSV
    var agencies = results.data.map(x => x.name);
    agencies = agencies.filter(n => {
      if (!(n == 'JBIC') && !(n == 'AIIB')) { return n }
    })
    // remove duplicates
    agencies = Array.from(new Set(agencies));
    for (agency of agencies) {
      var data = []
      for (row of results.data.filter(x => x.name == agency)) {
        if (!row.name || row.amount == 0) {
          continue;
        }
        data.push({x: row.year, y: row.amount, dfi: agency.toLowerCase()});
      }
      var lineColor = row.color;
      var dataset = {
        label: agency,
        data: data,
        borderColor: lineColor,
        fill: false
      }
      datasets.push(dataset);
    }
    //console.log(datasets);
    var ctx = document.getElementById('chart-global-agency');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['2013', '2014', '2015', '2016', '2017'],
        datasets: datasets,
      },
      options: {
        onClick: function(ev) {
          // https://stackoverflow.com/a/37123117/122400
          var activeElement = this.getElementAtEvent(ev);
          if (activeElement) {
            var agency = agencies[activeElement[0]._datasetIndex];
            window.location.href = '/dfi/' + agency.toLowerCase();
          }
        },
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          display: true,
          align: 'start',
          position: 'bottom',
          labels: {
            boxWidth: 24,
            padding: 16,
          },
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
        },
        tooltips: {      
          borderColor: 'transparent',  
          borderWidth: 0,
          cornerRadius: 2,
        }
      }
    });
  }
});

Papa.parse('/data/global-investment-region.csv', {
  download: true,
  header: true,
  complete: function(results) {
    // get region list from CSV
    var regions = results.data.map(x => x.region);
    var datasets = [];
    // remove duplicates
    regions = Array.from(new Set(regions));
    for (region of regions) {
      var data = []
      for (row of results.data.filter(x => x.region == region)) {
        // if (!row.name || row.amount == 0) {
        if (!row.region) {
          continue;
        }
        data.push({x: row.year, y: row.amount});
      }
      var dataset = {
        label: region,
        data: data,
        borderColor: row.color,
        fill: false
      }
      datasets.push(dataset);
    }
    var ctx = document.getElementById('chart-global-region');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['2013', '2014', '2015', '2016', '2017'],
        datasets: datasets,
      },
      options: {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          display: true,
          align: 'start',
          position: 'bottom',
          labels: {
            boxWidth: 24,
            padding: 16,
          },
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
        },
        tooltips: {      
          borderColor: 'transparent',  
          borderWidth: 0,
          cornerRadius: 2,
        }
      }
    });
  }
});

