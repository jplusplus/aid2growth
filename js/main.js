// mark up "No data" entries
var dds = document.querySelectorAll('dd');

for (var dd of dds) {
  if (dd.innerHTML.includes('No data')) {
    dd.classList.add('no-data');
  }
}
