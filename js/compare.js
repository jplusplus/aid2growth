urlParams = new URLSearchParams(window.location.search);
country1code = urlParams.get('country1');
country2code = urlParams.get('country2');

var series = [
  "population",
  "life_expectancy",
  "gdp",
  "gdp_growth",
  "gdp_per_capita",
  "capital_formation",
  "debt_stocks",
  "debt_service",
  "migration",
  "remittances",
  "investment_inflows",
  "investment",
  "net_oda",
]


function formatValue(value, series) {
  if (['gdp', 'gdp_per_capita', 'debt_stocks', 'remittances', 'investment', 'investment_inflows'].includes(series)) {
    value = parseFloat(value).toFixed(0)
    value = parseFloat(value).toLocaleString('en')
    if (value.startsWith('-')) {
      value = '-$' + value.replace('-', '')
    } else {
      value = '$' + value
    }
  } else if (['gdp_growth', 'debt_service'].includes(series)) {
    value = parseFloat(value).toFixed(2) + '%'
  } else if (value == '..') {
    value = 'No data'
  } else {
    value = parseFloat(value).toFixed(2)
  }
  return value
}


Papa.parse('/data/worldbank-clean.csv', {
  download: true,
  header: true,
  complete: function(results) {
    var country1data = results.data.filter(x => x.code3 == country1code.toUpperCase())
    var country2data = results.data.filter(x => x.code3 == country2code.toUpperCase())
    for (let sname of series) {
      let value1 = country1data.filter(x => x.series == sname)[0][2018]
      let value2 = country2data.filter(x => x.series == sname)[0][2018]
      if (document.querySelector("#country-1-" + sname)) {
        document.querySelector("#country-1-" + sname).innerHTML = formatValue(value1, sname)
        document.querySelector("#country-2-" + sname).innerHTML = formatValue(value2, sname)
      }
    }

  }
});

Papa.parse('/data/country-info.csv', {
  download: true,
  header: true,
  complete: function(results) {
    var country1info = results.data.filter(x => x["code3"] == country1code.toUpperCase())[0]
    var country2info = results.data.filter(x => x["code3"] == country2code.toUpperCase())[0]
    document.querySelector("#country-1-name").innerHTML = country1info.name
    document.querySelector("#country-2-name").innerHTML = country2info.name
    if (document.querySelector("#legend-country-1-name")) {
      document.querySelector("#legend-country-1-name").innerHTML = country1info.name
      document.querySelector("#legend-country-2-name").innerHTML = country2info.name
    }
    if (document.querySelector(".hero")) {
      // Background images must be in this order: country 2 first, country 1 second. CSS logic!
      document.querySelector(".hero").style = "background-image: url('/country-photos/" + country2code + ".jpg'), url('/country-photos/" + country1code + ".jpg') !important;"
    }
    createCompareCharts(country1info.name, country2info.name);
  }
});
