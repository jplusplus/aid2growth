/* Some defaults */
Chart.defaults.global.defaultFontFamily = '"Manrope", Roboto, DejaVu Sans, sans-serif';
Chart.defaults.global.defaultFontStyle = '500';
Chart.defaults.global.defaultFontSize = 12;
Chart.defaults.global.defaultFontColor = '#666';

/* Country aid per sector */
Papa.parse('/data/country-sector.csv', {
  download: true,
  header: true,
  complete: function(results) {
    var data = results.data.filter(x => x.name == countryName)[0]
    // values are strings, let's convert them to floats
    Object.keys(data).map(function(key, index) {
      data[key] = parseFloat(data[key]);
    });

    var ctx = document.getElementById('chart-country-sector');
    var myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: {
        labels: [
          ["Agriculture, Forestry,", "Fishing and Hunting"],
          ["Finance and", "Insurance"],
          ["Healthcare and", "Educational Services"],
          "Infrastructure",
          "Manufacturing",
          ["Mining, Quarrying,", "and Oil&Gas Extraction"],
          ["Tourism, Real Estate,", "and Retail Trade"],
          "Wholesale Trade",
          "Other",
        ],
        datasets: [{
          label: 'Aid received per sector',
          data: [
            data.agriculture,
            data.finance,
            data.healthcare,
            data.infrastructure,
            data.manufacturing,
            data.mining,
            data.tourism,
            data.wholesale,
            data.other,
          ],
          backgroundColor: '#f84919',
        }],
      },
      options: {
        maintainAspectRatio: false,
        legend: { display: false, },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
        },
        tooltips: {      
          borderColor: 'transparent',  
          borderWidth: 0,
          cornerRadius: 2,
        }
      }
    });
  }
});

/* Instruments of investment */
Papa.parse('/data/country-instruments.csv', {
  download: true,
  header: true,
  complete: function(results) {
    var data = results.data.filter(x => x.name == countryName)[0]
    // values are strings, let's convert them to floats
    Object.keys(data).map(function(key, index) {
      data[key] = parseFloat(data[key]);
    });

    var ctx = document.getElementById('chart-country-instruments');
    var myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: {
        labels: [
          "Loan",
          "Equity",
          "Insurance",
          "Other",
          "Unknown",
        ],
        datasets: [{
          label: 'Aid received per sector',
          data: [
            data.loan,
            data.equity,
            data.insurance,
            data.other,
            data.unknown,
          ],
          backgroundColor: '#fdd946',
        }],
      },
      options: {
        maintainAspectRatio: false,
        legend: { display: false, },
        scales: {
          xAxes: [{
            gridLines: {
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: { display: false },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
        },
        tooltips: {      
          borderColor: 'transparent',  
          borderWidth: 0,
          cornerRadius: 2,
        }
      }
    });
  }
});

