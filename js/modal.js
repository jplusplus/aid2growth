// Tailwind based modal: https://www.tailwindtoolbox.com/components/modal

var openmodal = document.querySelectorAll('.modal-open')
    for (var i = 0; i < openmodal.length; i++) {
      openmodal[i].addEventListener('click', function(event){
    	event.preventDefault()
        var el;
        if (event.target.tagName == 'I' || event.target.tagName == "SPAN") {
          el = event.target.parentElement
        } else {
          el = event.target
        }
        if (el.id.startsWith("embed-")) {
          var id = el.id.replace('embed-', '')
          var area = document.querySelector("#embed-code");
          if (el.id.startsWith("embed-country-")) {
            // countryCode is defined in a <script> tag in templates/country.html
            area.innerHTML = '<iframe src="http://a2g.jplusplus.pt/embed/country/?type=' + id + '&country=' + countryCode.toLowerCase() + '&countryname=' + countryName + '" width="640" height="280" frameborder="0"></iframe> \n<p><a href="http://a2g.jplusplus.pt/embed/country/' + countryCode.toLowerCase() + '">' + countryName + '</a> in Aid2Growth</p>';
          } else if (el.id.startsWith("embed-compare-")) {
            // same as above
            area.innerHTML = '<iframe src="http://a2g.jplusplus.pt/embed/compare/?type=' + id + '&country1=' + country1code + '&country2=' + country2code + '" width="640" height="280" frameborder="0"></iframe> \n<p><a href="http://a2g.jplusplus.pt/compare/?&country1=' + country1code + '&country2=' + country2code + '"> Compare</a> in Aid2Growth</p>';
          } else if (el.id.startsWith("embed-dfi-")) {
            area.innerHTML = '<iframe src="http://a2g.jplusplus.pt/embed/dfi/?name=' + dfiCode + '&type=' + id + '" width="640" height="280" frameborder="0"></iframe> \n<p><a href="http://a2g.jplusplus.pt/dfi/' + dfiCode + '"> Compare</a> in Aid2Growth</p>';
          } else {
            area.innerHTML = '<iframe src="http://a2g.jplusplus.pt/embed/?name=' + dfiCode + '&type=' + id + '" width="640" height="280" frameborder="0"></iframe> \n<p><a href="http://a2g.jplusplus.pt">Aid2Growth/a></p>';
          }
          openEmbedModal()
        } else {
          openCompareModal()
        }
      })
    }
    
    /* iframe for the other embeds -- needs to be reviewed
          } else if (el.id.startsWith("embed-compare-")) {
            area.innerHTML = '<iframe src="http://a2g.jplusplus.pt/embed/compare/?type=' + id + '&country1=' + country1code + '&country2=' + country2code + '" width="640" height="280" frameborder="0"></iframe> \n<p><a href="http://a2g.jplusplus.pt/compare/?&country1=' + country1code + '&country2=' + country2code + '"> Compare</a> in Aid2Growth</p>';
          } else {
            area.innerHTML = '<iframe src="http://a2g.jplusplus.pt/embed/?type=' + id + '" width="640" height="280" frameborder="0"></iframe> \n<p><a href="http://a2g.jplusplus.pt">Aid2Growth/a></p>';
    */
    
    var overlay = document.querySelectorAll('.modal-overlay')
    for (var i = 0; i < overlay.length; i++) {
      overlay[i].addEventListener('click', closeModals)
    }
    
    var closemodal = document.querySelectorAll('.modal-close')
    for (var i = 0; i < closemodal.length; i++) {
      closemodal[i].addEventListener('click', closeModals)
    }
    
    document.onkeydown = function(evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
    	isEscape = (evt.key === "Escape" || evt.key === "Esc")
      } else {
    	isEscape = (evt.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active')) {
    	closeModals()
      }
    };
    
    const body = document.querySelector('body')
    const embedModal = document.querySelector('#embed-modal')
    const compareModal = document.querySelector('#compare-modal')

    function openEmbedModal() {
      console.log("opening embed modal!");
      embedModal.classList.remove('hide')
      body.classList.add('modal-active')
    }
    function closeEmbedModal() {
      embedModal.classList.add('hide')
      body.classList.remove('modal-active')
    }
    function openCompareModal() {
      compareModal.classList.remove('hide')
      body.classList.add('modal-active')
    }
    function closeCompareModal() {
      compareModal.classList.add('hide')
      body.classList.remove('modal-active')
    }
    function closeModals() {
      embedModal.classList.add('hide')
      if (compareModal) {
        compareModal.classList.add('hide')
      }
      body.classList.remove('modal-active')
    }



