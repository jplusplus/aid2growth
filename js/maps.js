/* Mapas e charts do index */

var map;
var countries = {};


Papa.parse('/data/country-info.csv', {
  download: true,
  header: true,
  complete: function(results) {
    mapHighlights = {}
    for (var row of results.data) {
      countries[row.code3] = row.name;
      mapHighlights[row.code3] = {fillKey: 'available'};
    }

    map = new Datamap({
      element: document.getElementById('map'),
      projection: 'mercator',
      fills: {
        available: "#06576a",
        defaultFill: "#999"
      },
      data: mapHighlights,
      geographyConfig: {
        // hideAntarctica: true,
        borderWidth: 1,
        borderOpacity: 1,
        borderColor: '#eaeceb',
        /* configure text on popup
         popupTemplate: function(geography, data) {
           return '<div class="hoverinfo"><strong>' + geography.properties.name + '</strong><a href="">Go to country</a></div>';
         },*/
        popupOnHover: true, //disable the popup while hovering but missing click!
        highlightOnHover: function(geo) {
          if (geo.fillKey) {
            return true;
          }
          return false;
        },
        highlightFillColor: function(geo) {
          if (geo.fillKey) {
            return '#078492';
          } else {
            return '#999';
          }
        },
        highlightBorderColor: false,
      },
      done: function(datamap) {
        // add links to each country page
        datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
          var id = geography.id.toLowerCase();
          if (id.toUpperCase() in countries) {
            location.href = '/country/' + id;
          }
        });

        // add DFI bubbles
        Papa.parse('/data/dfi-info.csv', {
          download: true,
          header: true,
          complete: function(results) {
            var agencies = [];
            for (var row of results.data) {
              if (!row.acronym || row.acronym == 'JBIC' || row.acronym == 'AIIB') {
                continue;
              }
              agencies.push({
                name: row.acronym,
                latitude: row.lat,
                longitude: row.lon,
                fillKey: row.acronym,
                radius: 20,
              });
              map.options.fills[row.acronym] = row.color
            }
            map.bubbles(agencies);
            map.svg.selectAll('.datamaps-bubble').on('click', function(dfi) {
              var id = dfi.name.toLowerCase();
              location.href = '/dfi/' + id;
            });
          }
        });
      }
    });
  }
});

