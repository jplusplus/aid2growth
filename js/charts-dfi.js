/* Some defaults */
Chart.defaults.global.defaultFontFamily = '"Manrope", Roboto, DejaVu Sans, sans-serif';
Chart.defaults.global.defaultFontStyle = '500';
Chart.defaults.global.defaultFontSize = 12;
Chart.defaults.global.defaultFontColor = '#666';

/* Investments per region */
Papa.parse('/data/dfi-region.csv', {
  download: true,
  header: true,
  complete: function(results) {
    // get DFI list from CSV
    var datasets = [];

    var data = []
    for (row of results.data.filter(x => x.dfi.toLowerCase() == dfiName.toLowerCase())) {
      // if (!row.name || row.amount == 0) {
      if (!row.dfi) {
        continue;
      }
      data.push({x: row.region, y: row.amount});
    }
    // var lineColor = row.color;
    var dataset = {
      // label: dfiName,
      data: data,
      // borderColor: lineColor,
      fill: true,
      backgroundColor: '#078492',
    }
    datasets.push(dataset);
    
    console.log(datasets);
    var ctx = document.getElementById('chart-dfi-region');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [
          'Asia and the Pacific',
          'Europe and Central Asia',
          'Global',
          'Latin America and Caribbean',
          'Middle East and North Africa',
          'North America',
          'Regional (Africa)',
          'Sub-Saharan Africa',
        ],
        datasets: datasets,
      },
      options: {
        maintainAspectRatio: false,
        legend: { display: false, },        
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
        },
        tooltips: {      
          borderColor: 'transparent',  
          borderWidth: 0,
          cornerRadius: 2,
        }
      }
    });
  }
});

/* Investments per region */
Papa.parse('/data/dfi-instrument.csv', {
  download: true,
  header: true,
  complete: function(results) {
    // get DFI list from CSV
    var dfis = results.data.map(x => x.dfi);
    var datasets = [];
    // remove duplicates
    
    var data = []
    for (row of results.data.filter(x => x.dfi.toLowerCase() == dfiName.toLowerCase())) {
      // if (!row.name || row.amount == 0) {
      if (!row.dfi) {
        continue;
      }
      data.push({x: row.instrument, y: row.amount});
    }
    // var lineColor = row.color;
    var dataset = {
      // label: dfiName,
      data: data,
      // borderColor: lineColor,
      fill: true,
      backgroundColor: '#ffdd55',
    }
    datasets.push(dataset);
    
    var ctx = document.getElementById('chart-dfi-instrument');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [
          'Equity',
          'Loan',
          'Insurance',
          'Other',
          'Unknown',
        ],
        datasets: datasets,
      },
      options: {
        maintainAspectRatio: false,
        legend: { display: false, },
        scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
        },
        tooltips: {      
          borderColor: 'transparent',  
          borderWidth: 0,
          cornerRadius: 2,
        }
      }
    });
  }
});

