/* Some defaults */
Chart.defaults.global.defaultFontFamily = '"Manrope", Roboto, DejaVu Sans, sans-serif'
Chart.defaults.global.defaultFontStyle = '500'
Chart.defaults.global.defaultFontSize = 12
Chart.defaults.global.defaultFontColor = '#666'


function createCompareCharts(countryName1, countryName2) {
  /* Country aid per sector */
  Papa.parse('/data/country-sector.csv', {
    download: true,
    header: true,
    complete: function(results) {
      var country1data = results.data.filter(x => x.name == countryName1)[0]
      var country2data = results.data.filter(x => x.name == countryName2)[0]
      console.log(country1data)
      console.log(country2data)
      // values are strings, let's convert them to floats
      Object.keys(country1data).map(function(key, index) {
        country1data[key] = parseFloat(country1data[key]);
      });
      Object.keys(country2data).map(function(key, index) {
        country2data[key] = parseFloat(country2data[key]);
      });

      var ctx = document.getElementById('chart-compare-sector');
      var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: [
            ["Agriculture, Forestry,", "Fishing and Hunting"],
            ["Finance and", "Insurance"],
            ["Healthcare and", "Educational Services"],
            "Infrastructure",
            "Manufacturing",
            ["Mining, Quarrying,", "and Oil&Gas Extraction"],
            ["Tourism, Real Estate,", "and Retail Trade"],
            "Wholesale Trade",
            "Other",
          ],
          datasets: [{
            label: countryName1,
            data: [
              country1data.agriculture,
              country1data.finance,
              country1data.healthcare,
              country1data.infrastructure,
              country1data.manufacturing,
              country1data.mining,
              country1data.tourism,
              country1data.wholesale,
              country1data.other,
            ],
            backgroundColor: '#06576a',
              borderWidth: 0,
          },
            {
              label: countryName2,
              data: [
                country2data.agriculture,
                country2data.finance,
                country2data.healthcare,
                country2data.infrastructure,
                country2data.manufacturing,
                country2data.mining,
                country2data.tourism,
                country2data.wholesale,
                country2data.other,
              ],
              backgroundColor: '#ffdd55',
              borderWidth: 0,
            }
          ],
        },
        options: {
          maintainAspectRatio: false,
          legend: { display: false, },
          scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
          }
        }
      });
    }
  });


  Papa.parse('/data/country-instruments.csv', {
    download: true,
    header: true,
    complete: function(results) {
      var country1data = results.data.filter(x => x.name == countryName1)[0]
      var country2data = results.data.filter(x => x.name == countryName2)[0]
      // values are strings, let's convert them to floats
      Object.keys(country1data).map(function(key, index) {
        country1data[key] = parseFloat(country1data[key]);
      });
      Object.keys(country2data).map(function(key, index) {
        country2data[key] = parseFloat(country2data[key]);
      });

      var ctx = document.getElementById('chart-compare-instruments');
      var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: [
            "Loan",
            "Equity",
            "Insurance",
            "Other",
            "Unknown",
          ],
          datasets: [{
            label: countryName1,
            data: [
              country1data.loan,
              country1data.equity,
              country1data.insurance,
              country1data.other,
              country1data.unknown,
            ],
            backgroundColor: '#06576a',
            borderWidth: 0,
          },
            {
              label: countryName2,
              data: [
                country2data.loan,
                country2data.equity,
                country2data.insurance,
                country2data.other,
                country2data.unknown,
              ],
              backgroundColor: '#ffdd55',
              borderWidth: 0,
            }],
        },
        options: {
          maintainAspectRatio: false,
          legend: { display: false, },
          scales: {
          xAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#ccc',
              lineWidth: 1,
              zeroLineColor: '#ccc',
              zeroLineWidth: 1,
              borderDash:[3, 5],
             },
            ticks: {
              display: true,
              beginAtZero: true,
              fontStyle: '600',
              fontColor: '#666',
              padding: 10,
            },
          }],
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
              display: true,
              padding: 5,
            }
          }]
          }
        }
      });
    }
  });
}
