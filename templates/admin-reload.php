<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
<?php

  function execPrint($command) {
    $result = array();
    exec($command, $result);
    foreach ($result as $line) {
      print($line . "\n");
    }
  }

  $referrer = $_SERVER['HTTP_REFERER'];
  if (strpos( $referrer, "http://a2g.jplusplus.pt/" ) === 0){
    // Get the latest version of the repository
    print("<pre>");
    execPrint("cd ~/repos/aid2growth && make update && make local-deploy");
    print("</pre>");
  } else {
    echo "You shouldn't be here!";
  }

?>


  </body>
</html>

