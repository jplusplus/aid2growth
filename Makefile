DEBUG = True
BUILD_DIR = dist

SSH_HOSTNAME = jplusplus
SSH_DIR = /home/jplusplus/apps/a2g-php/
SSH_PATH = $(SSH_HOSTNAME):$(SSH_DIR)

SASS_FILES_DIR = css
JS_FILES_DIR = js
FONTS_DIR = fonts
IMAGES_DIR = img
CONTENT_IMAGES_DIR = content/img
JS_VENDOR_FILES_DIR = js/vendor
TARGET_CSS_DIR = $(BUILD_DIR)/css
TARGET_JS_DIR = $(BUILD_DIR)/js
TARGET_JS_VENDOR_DIR = $(BUILD_DIR)/js/vendor
TARGET_FONTS_DIR = $(BUILD_DIR)/fonts
TARGET_IMAGES_DIR = $(BUILD_DIR)/img

# CSS via Sass
SASS_FILES = $(wildcard $(SASS_FILES_DIR)/*.scss)
CSS_FILES=$(patsubst $(SASS_FILES_DIR)/%.scss,$(TARGET_CSS_DIR)/%.css,$(SASS_FILES))
SASS=./lib/sass
ifeq ($(DEBUG),True)
    SASS_FLAGS = --style expanded
else
    SASS_FLAGS = --style compressed --quiet
endif

# helper python commands
ACTIVATE=. `pwd`/.env/bin/activate

ifeq ($(shell hostname),opal5.opalstack.com)
        PANDOC = /home/jplusplus/bin/pandoc
else
        PANDOC = pandoc
endif


build: dir sass css js fonts data images html

dir:
	mkdir -p $(BUILD_DIR) $(TARGET_CSS_DIR) $(TARGET_JS_DIR) $(TARGET_JS_VENDOR_DIR) $(TARGET_FONTS_DIR) $(TARGET_IMAGES_DIR) dist/data/ dist/posts/

sass: $(CSS_FILES)
$(TARGET_CSS_DIR)/%.css: $(SASS_FILES_DIR)/%.scss
	$(SASS) $(SASS_FLAGS) $< > $@

css:
	cp $(SASS_FILES_DIR)/*.css $(TARGET_CSS_DIR)

js:
	cp $(JS_FILES_DIR)/*.js $(TARGET_JS_DIR)
	cp $(JS_VENDOR_FILES_DIR)/*.js $(TARGET_JS_VENDOR_DIR)

data:
	cp data/* dist/data/ -r

update: update-data update-content update-images

update-data:
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=0'     >| data/dfi-info.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=407044220' >| data/country-info.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=1597669213' >| data/projects.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=1740285978' >| data/country-instruments.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=2016004383' >| data/country-sector.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=2117217666' >| data/aid-per-country.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=1949583618' >| data/global-investment-agency.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=918950564' >| data/global-investment-yearly.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=162630263' >| data/global-investment-region.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=551040842' >| data/dfi-region.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=1230610223' >| data/dfi-instrument.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1HhucObLtgEuO3swOIIK2lly2sF2cXAptxYg7r_3G1No/gviz/tq?tqx=out:csv&gid=1702181194' >| data/country-descriptions.csv

update-content:
	mkdir -p content
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 lib/html_clean.py 'https://docs.google.com/document/export?format=html&id=1Em1KAmWndr6mJy_reGjL6Ej1slvgz-3BSZwJDQrZbKs' >| content/about.html
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 lib/html_clean.py 'https://docs.google.com/document/export?format=html&id=1RpjNi-fYbeGs04e1mNqm_H6qLVaUGmGWYzOgUg792AE' >| content/methodology.html
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 lib/html_clean.py 'https://docs.google.com/document/export?format=html&id=1dH0shCi48cJkJVBQ5Gbg4B5wq2gebHabABf9f95QJdU' >| content/links.html
	curl --silent 'https://docs.google.com/spreadsheets/d/13KYpAVmVZLIZ34TBL50n_UXPsxd7Ok_W3r3FADY7Im0/gviz/tq?tqx=out:csv' >| content/reports.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1PLNaToDbnNXRkgDwLgFC5z5yF2EKjawvzxrM0cw4fTs/gviz/tq?tqx=out:csv' >| content/glossary.csv
	curl --silent 'https://docs.google.com/spreadsheets/d/1dp54fE23USsSRNFsWg4isfYF5vzGowLNBfnHX4iqRU4/gviz/tq?tqx=out:csv' >| content/team.csv 
	@echo $(PANDOC)
	$(PANDOC) content/about.html -f html -t markdown -o content/about.md
	$(PANDOC) content/methodology.html -f html -t markdown -o content/methodology.md
	$(PANDOC) content/links.html -f html -t markdown -o content/links.md

update-images:
	$(ACTIVATE); cd lib; python3 get_photos.py; cd ..

fonts:
	cp -r $(FONTS_DIR)/* $(TARGET_FONTS_DIR)

images:
	cp -r $(IMAGES_DIR)/* $(TARGET_IMAGES_DIR)
	# cp $(CONTENT_IMAGES_DIR)/* $(TARGET_IMAGES_DIR) -r

html:
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 lib/jinja_generate.py
	# cp *.html $(BUILD_DIR)
	mkdir -p dist/admin/
	cp templates/admin.html dist/admin/index.html
	cp templates/admin-reload.php dist/admin/reload.php
	cp favicon.png $(BUILD_DIR)

serve:
	make -j2 runserver watch
runserver:
	cd dist; python3 -m http.server 8000
watch:
	watch make
clean:
	rm -fr $(BUILD_DIR)
install:
	python3 -m venv .env
	$(ACTIVATE); pip install -r requirements.txt

deploy: build
	rsync --compress --checksum --progress --recursive --update dist/ $(SSH_PATH)
	ssh jplusplus -C "cd ~/repos/aid2growth; git pull"
dry-deploy: build
	rsync --dry-run --compress --checksum --progress --recursive --update dist/ $(SSH_PATH)
local-deploy: build
	rsync --compress --checksum --progress --recursive --update dist/ ~/apps/a2g-php/

.PHONY: clean install watch serve html css js dir fonts images data reports
